//Jantar de Filósofos sem controle de impasses.

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>
#include <windows.h>

#define NUMFILO 5

pthread_t filosofo [NUMFILO] ;  // thread Filósofos
sem_t hashi [NUMFILO] ; // um semáforo para cada palito (iniciam em 1)

//espaços para separar as colunas de impressão
char *space[] = {"\t", "\t\t\t", "\t\t\t\t\t", "\t\t\t\t\t\t\t", "\t\t\t\t\t\t\t\t\t" } ;

//espera um tempo aleatório entre 0 e n (float)
void espera (int n)
{
     Sleep (n);
}

// filósofo comendo
void come (int f)
{
    printf ("%sCOMENDO\n", space[f]) ;
//  espera (700);
}

//filósofo meditando
void medita (int f)
{
    printf ("%sMEDITANDO\n", space[f]) ;
//  espera (700) ;
}

// pega o hashi
void pega_hashi (int f, int h)
{
    printf ("%sQUER H%d\n", space[f], h) ;
    sem_wait (&hashi [h]) ;
    printf ("%sPEGOU H%d\n", space[f], h) ;
}

//larga o hashi
void larga_hashi (int f, int h)
{
    printf ("%sLARGA H%d\n", space[f], h) ;
    sem_post (&hashi [h]) ;
}

// corpo da thread filósofo
void *threadFilosofo (void *arg)
{
    int i = (long int) arg ;
    while (1)
    {
        medita (i) ;
        pega_hashi (i, i) ;
        pega_hashi (i, (i+1) % NUMFILO) ;
        come (i) ;
        larga_hashi (i, i) ;
        larga_hashi (i, (i+1) % NUMFILO) ;
    }
    pthread_exit (NULL) ;
} 

// programa principal
int main (int argc, char *argv[])
{
    long i, status ;

    // para o print não se confundir com a thread
    setvbuf (stdout, 0, _IONBF, 0) ;

    // inicia os hashis
    for (i=0; i<NUMFILO; i++)
        sem_init (&hashi[i], 0, 1) ;

    //inicia os Filósofos
    for (i=0; i<NUMFILO; i++)
    {
        status = pthread_create (&filosofo[i], NULL, threadFilosofo, (void *) i) ;
        if (status)
        {
            perror ("pthread_create") ;
            exit (1) ;
        }
    }

    //encerramento da main
    pthread_exit (NULL) ;
}